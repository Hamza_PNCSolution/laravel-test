<?php

namespace App\Http\Controllers;

use App\Company;
use App\Product;
use Illuminate\Http\Request;

class PublicAPIController extends Controller
{
    public function companies()
    {
        $companies = Company::latest()->paginate(10);

        return response()->json($companies, 200);
    }
}
