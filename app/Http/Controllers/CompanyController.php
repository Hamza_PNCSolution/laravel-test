<?php

namespace App\Http\Controllers;

use App\Company;
use App\Notifications\NewCompany;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::latest()->paginate(10);
        return response()->json($companies, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|max:255|unique:products,title',
            'email' => 'required|unique:companies,email',
            'logo' => 'required',
            'website' => 'required',
        ]);

        $company = Company::create([
            'name' => $request->name,
            'email' => $request->email,
            'website' => $request->website,
        ]);

        if ($request->logo) {
            $imageName = time() . '_' . uniqid() . '.' . $request->logo->getClientOriginalExtension();
            $request->logo->move(public_path('storage/logo'), $imageName);
            $company->logo = '/storage/logo/' . $imageName;
            $company->save();
        }

        if($company){

            $user = User::first();
            $company_name = $request->name;
            $user->notify(new NewCompany($company_name));
        }

        return response()->json($company, 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        return response()->json($company, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Company $company)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required',
            'logo' => 'required',
            'website' => 'required',
        ]);

        $company->update([
            'name' => $request->name,
            'email' => $request->email,
            'website' => $request->website,
        ]);

        if ($request->logo) {
            $logoName = time() . '_' . uniqid() . '.' . $request->logo->getClientOriginalExtension();
            $request->logo->move(public_path('storage/company'), $logoName);
            $company->logo = '/storage/company/' . $logoName;
            $company->save();
        }

        return response()->json($company, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        if ($company) {
            $companyImage = $company->image;
            $imagePath = public_path($companyImage);

            if ($companyImage && file_exists($imagePath)) {
                unlink($imagePath);
            }

            $company->delete();
        } else {
            return response()->json('Company not found.', 404);
        }
    }
}
