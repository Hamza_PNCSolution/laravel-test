import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import Home from '../pages/home.vue'
import CompnayList from '../pages/company/index.vue'
import CreateCompany from '../pages/company/create.vue'
import EditCompany from '../pages/company/edit.vue'
import employeeList from '../pages/employee/index.vue'
import Createemployee from '../pages/employee/create.vue'
import Editemployee from '../pages/employee/edit.vue'

// Dashboard Component
import Dashboard from '../pages/dashboard/index.vue'
import UserProfile from '../pages/dashboard/profile.vue'

// Authentication File
import Login from '../pages/auth/Login.vue'
import Signup from '../pages/auth/Signup.vue'

const routes = new VueRouter({
    mode: 'history',
    linkExactActiveClass: 'active',
    routes: [
        {
            path: '/',
            component: Home,
            name: 'home',
        },
        {
            path: '/company',
            component: CompnayList,
            name: 'company-list',
        },
        {
            path: '/company/create',
            component: CreateCompany,
            name: 'create-company',
        },
        {
            path: '/company/edit/:id',
            component: EditCompany,
            name: 'edit-company',
        },
        {
            path: '/employee',
            component: employeeList,
            name: 'employee-list',
        },
        {
            path: '/employee/create',
            component: Createemployee,
            name: 'create-employee',
        },
        {
            path: '/employee/edit/:id',
            component: Editemployee,
            name: 'edit-employee',
        },
        {
            path: '/auth/login',
            component: Login,
            name: 'login',
            meta: {
                requiresVisitor: true,
            }
        },
        {
            path: '/auth/signup',
            component: Signup,
            name: 'signup',
            meta: {
                requiresVisitor: true,
            }
        },
        {
            path: '/dashboard',
            component: Dashboard,
            name: 'dashboard',
            meta: {
                requiresAuth: true,
            }
        },
        {
            path: '/dashboard/profile',
            component: UserProfile,
            name: 'user-profile',
            meta: {
                requiresAuth: true,
            }
        },
    ]
});

export default routes;
